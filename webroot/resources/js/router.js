import Vue from 'vue';
import VueRouter from 'vue-router';

import Assets from './pages/Assets.vue';
import Login from './pages/Login.vue';
import Register from './pages/Register.vue';
import Users from './pages/Users.vue';
import MyProfile from './pages/MyProfile.vue';
import MyAssets from './pages/MyAssets.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'assets',
            component: Assets
        },
        {
            path: '/my-assets',
            name: 'my-assets',
            component: MyAssets
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/users',
            name: 'users',
            component: Users
        },
        {
            path: '/my-profile',
            name: 'my-profile',
            component: MyProfile
        },
    ]
});

export default router;
