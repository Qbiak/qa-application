export default function loggedInUser() {
    return {
        // Cookie handler for logged in user (Auth)
        setLoggedInUser(userData) {
            localStorage.setItem('qaApp.user', JSON.stringify(userData));

            window.dispatchEvent(new CustomEvent('user-localstorage-changed', {
                detail: {
                    storage: JSON.parse(localStorage.getItem('qaApp.user'))
                }
            }));
        },
        getLoggedInUser() {
            return JSON.parse(localStorage.getItem('qaApp.user'));
        },
        removeLoggedInUser() {
            localStorage.removeItem('qaApp.user');

            window.dispatchEvent(new CustomEvent('user-localstorage-changed', {
                detail: {
                    storage: null
                }
            }));
        },
    }
}
