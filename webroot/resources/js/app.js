require('./bootstrap');

window.Vue = require('vue').default;

import router from './router';
import App from './layouts/App.vue';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faTrash,
    faEdit,
    faLaptop,
    faUsers,
    faUser,
    faSignInAlt,
    faSignOutAlt,
    faFileSignature,
    faAngleLeft,
    faAngleRight,
    faAngleDoubleLeft,
    faAngleDoubleRight,
    faSave,
    faBan,
    faPlus,
    faLaptopHouse
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import VueSweetalert2 from 'vue-sweetalert2';


library.add(faTrash, faEdit, faLaptop, faUsers, faUser, faSignInAlt, faSignOutAlt, faFileSignature, faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight, faSave, faBan, faPlus, faLaptopHouse)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

Vue.component('Spinner', require('./components/Spinner.vue').default);
Vue.component('BaseButton', require('./components/BaseButton.vue').default);
Vue.use(VueToast, {position: 'bottom-left'});
Vue.use(VueSweetalert2);
Vue.prototype.$eventBus = new Vue();

window.$eventBus = Vue.prototype.$eventBus

axios.defaults.withCredentials = true

axios.interceptors.response.use(
    response => response,
    error => {
        if (error.response.status === 401 && error.response?.data?.message === 'Invalid Login Details') {
            // Display Invalid Login message
            Vue.$toast.open({
                message: error.response.data.message,
                type: 'error',
            });
        } else if (error.response.status === 401) {
            // Unauthorized and redirect to login screen, or incorrect permissions
            Vue.$toast.open({
                message: error.response.data.message,
                type: 'error',
            });
            window.location = '/login'
        } else if (error.response.status === 403) {
            // User does not have permission
            Vue.$toast.open({
                message: error.response.data.message,
                type: 'error',
            });
        } else if (error.response.status === 422 && error.response?.data?.errors) {
            // Display Validation Error Messages
            for (const key in (error.response?.data?.errors)) {
                Vue.$toast.open({
                    message: error.response?.data?.errors[key]?.[0],
                    type: 'error',
                });
            }
        } else {
            // Display Other Errors
            Vue.$toast.open({
                message: error.response.data.message,
                type: 'error',
            });
        }
        return Promise.reject(error)
    }
)

const app = new Vue({
    router,
    el: '#app',
    render: h => h(App)
});
