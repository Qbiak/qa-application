<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('/storage/images/favicon2.ico') }}">
    <title>IT Asset Manager</title>

    <link href="{{ asset('css/app.css') }}"  rel="stylesheet">
</head>

<body class="antialiased">

<div id="app"></div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
</body>

</html>
