@servers(['production' => '167.172.52.142', 'staging' => '188.166.146.69'])

@setup
$currentDir = '/var/qa-application';
$containerDir = '/var/www/html';

function logMessage($message, $single = false) {
if($single == false){
return "echo '\033[32m" .$message. "\033[0m';\n";
}
}
@endsetup

@story('deploy', ['on' => ['production']])
git
container
cache
@endstory

@story('deploy-staging', ['on' => ['staging']])
git-staging
container-staging
cache-staging
@endstory

@task('container', ['on' => ['production']])
{{ logMessage("Updating Docker container on Production Server")}}
cd {{ $currentDir }}

docker-compose -f docker-compose.prod.yml pull
docker-compose -f docker-compose.prod.yml up --build -d --remove-orphans --force-recreate
docker-compose -f docker-compose.prod.yml down
docker volume rm qa-application_container-data
docker-compose -f docker-compose.prod.yml up -d
@endtask

@task('container-staging', ['on' => ['staging']])
{{ logMessage("Updating Docker container on Staging Server")}}
cd {{ $currentDir }}

docker-compose -f docker-compose.prod.yml pull
docker-compose -f docker-compose.prod.yml up --build -d --remove-orphans --force-recreate
docker-compose -f docker-compose.prod.yml down
docker volume rm qa-application_container-data
docker-compose -f docker-compose.prod.yml up -d
@endtask

@task('git', ['on' => ['production']])
{{ logMessage("Updating Git Repository on Production Server")}}
cd {{ $currentDir }}

git fetch --prune
git pull
@endtask

@task('git-staging', ['on' => ['staging']])
{{ logMessage("Updating Git Repository on Staging Server")}}
cd {{ $currentDir }}

git fetch --prune
git pull
@endtask

@task('cache', ['on' => ['production']])
{{ logMessage("Clearing and building new cache on Production Server")}}

cd {{ $currentDir }}

docker-compose exec -T app bash

php {{ $containerDir }}/artisan optimize

php {{ $containerDir }}/artisan event:cache

php {{ $containerDir }}/artisan storage:link

chmod -R 755 {{ $containerDir }}/storage/
@endtask

@task('cache-staging', ['on' => ['staging']])
{{ logMessage("Clearing and building new cache on Staging Server")}}

cd {{ $currentDir }}

docker-compose exec -T app bash

php {{ $containerDir }}/artisan optimize

php {{ $containerDir }}/artisan event:cache

php {{ $containerDir }}/artisan storage:link

chmod -R 755 {{ $containerDir }}/storage/
@endtask

@finished
if (!$exitCode) {
echo "\033[32m" . "All Tasks Complete" . "\033[0m" . "\n";
}
@endfinished
