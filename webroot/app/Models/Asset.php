<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Asset
 *
 * @property int $id
 * @property int $type_id
 * @property string $serial_number
 * @property string $current_state
 * @property int|null $assignee_id
 * @property int $assignor_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\User|null $assignee
 * @property-read \App\Models\User $assignor
 * @property-read \App\Models\Type $type
 * @method static \Database\Factories\AssetFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Asset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Asset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Asset query()
 * @mixin \Eloquent
 */
class Asset extends Model
{
    use HasFactory;

    /**
     * Attributes that are mass assignable.
     * Protects from injections to database.
     *
     * @var string[]
     */
    protected $fillable = [
        'type_id',
        'serial_number',
        'current_state',
        'assignee_id',
    ];

    /**
     * Selects related attributes to be fetched with eager loading.
     *
     * @var string[]
     */
    protected $with = ['assignee:id,name', 'type:id,asset_type'];

    /**
     * Api path
     *
     * @return string
     */
    public function path()
    : string
    {
        return 'api/assets/'.$this->id;
    }

    /**
     * One to many relationship with user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignee()
    : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'assignee_id');
    }

    /**
     * One to many relationship with type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    /**
     * Setter for type_id attribute
     *
     * @param  string  $type
     *
     * @return void
     */
    public function setTypeIdAttribute(string $type)
    : void {
        $this->attributes['type_id'] = Type::firstOrCreate([
            'asset_type' => $type,
        ])->id;
    }

    /**
     * Setter for current_state attribute
     *
     * @param  string  $state
     *
     * @return void
     */
    public function setCurrentStateAttribute(string $state)
    : void {
        $states = [
            'Available',
            'Allocated',
            'Damaged',
            'Reserved'
        ];

        if (in_array($state, $states, true)) {
            $this->attributes['current_state'] = $state;
        } else {
            $this->attributes['current_state'] = 'N/A';
        }
    }

    /**
     * Conditional setter for assignee_id attribute
     *
     * @param  int|null  $assignee
     *
     * @return void
     */
    public function setAssigneeIdAttribute(int|null $assignee)
    : void {
        if ($this->attributes['current_state'] === 'Available' || $this->attributes['current_state'] === 'Damaged') {
            $this->attributes['assignee_id'] = null;
        } else {
            $this->attributes['assignee_id'] = $assignee;
        }
    }
}
