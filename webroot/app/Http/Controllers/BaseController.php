<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

/**
 * Class BaseController
 *
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     * Successful JSON Response
     *
     * @param  mixed  $result
     * @param  string  $message
     * @param  int  $code
     *
     * @return JsonResponse
     */
    public function sendResponse($result, string $message, int $code = 200)
    : JsonResponse {
        $response = [
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, $code);
    }

    /**
     * Error JSON Response
     *
     * @param  string  $error
     * @param  string|array  $errorMessages
     * @param  int  $code
     *
     * @return JsonResponse
     */
    public function sendError(string $error, $errorMessages = [], int $code = 500)
    : JsonResponse {
        $response = [
            'message' => $error,
        ];

        if ( ! empty($errorMessages)) {
            $response['errors'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
