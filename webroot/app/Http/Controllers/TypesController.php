<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Models\Type;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypesController extends BaseController
{
    /**
     * Retrieve types list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    : JsonResponse {
        // Get data
        $types = Type::all()->toArray();

        // Send response
        return $this->sendResponse($types, 'Types retrieved successfully.');
    }

    /**
     * Create new type
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    : JsonResponse {
        $requestData = $request->all();

        // Validation rules for the attributes
        $validatedData = Validator::make($requestData, [
            'asset_type' => 'required|string|min:3|max:15',
        ]);

        // Validation fail response
        if ($validatedData->fails()) {
            return $this->sendError('Validation Error.', $validatedData->errors(), 422);
        }

        // Creating new type
        $type             = new Type;
        $type->asset_type = $requestData['asset_type'];
        $type->save();

        // Send successful response
        return $this->sendResponse($type, 'Type created successfully.', 201);
    }
}
