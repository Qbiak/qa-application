<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use App\Rules\Recaptcha;

class AuthController extends BaseController
{
    /**
     * Register User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rules\Recaptcha  $recaptcha
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request, Recaptcha $recaptcha)
    : JsonResponse {
        $requestData = $request->all();

        // Validate the request
        $validatedData = Validator::make($requestData, [
            'name'      => 'required|max:255|min:3',
            'email'     => 'required|email|max:255|min:8|unique:users',
            'recaptcha' => ['required', $recaptcha],
            'password'  => [
                'required',
                'confirmed',
                'max:255',
                Password::min(8)->letters()->mixedCase()->numbers()->symbols()->uncompromised(),
            ],
        ]);

        // Validation fail response
        if ($validatedData->fails()) {
            return $this->sendError('Validation Error.', $validatedData->errors(), 422);
        }

        // Create new user
        $user           = new User;
        $user->name     = $requestData['name'];
        $user->email    = $requestData['email'];
        $user->password = $requestData['password'];
        $user->save();

        // Send response
        return $this->sendResponse($user->toArray(), 'Registered Successfully', 201);
    }

    /**
     * Login User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rules\Recaptcha  $recaptcha
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request, Recaptcha $recaptcha)
    : JsonResponse {
        // Validate the request
        $attributes = $request->validate([
            'email'     => 'required',
            'password'  => 'required',
            'recaptcha' => ['required', $recaptcha],
        ]);

        // Attempt to authenticate and log in the user based on the provided credentials
        if (Auth::guard()->attempt($request->only(['email', 'password']))) {
            $request->session()->regenerate();

            return response()->json(['data' => Auth::user()]);
        }

        // Auth failed
        return response()->json(['message' => 'Invalid Login Details'], 401);
    }

    /**
     * Logout user
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    : JsonResponse {
        auth()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->json(['message' => 'Logged Out Successfully']);
    }
}
