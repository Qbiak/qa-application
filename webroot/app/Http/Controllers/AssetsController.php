<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Type;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AssetsController extends BaseController
{
    /**
     * Retrieve paginated user array
     *
     * @param  string  $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(string $status)
    : JsonResponse {
        // Get user ID from logged in user
        $user = User::where('id', (int) Auth::id())->first();

        if ($status === 'All') {
            $status = '%';
        }

        if ($status === 'own') {
            // Rule to fetch only own assets
            $assets          = Asset::where('assignee_id', Auth::id())->paginate(10)->toArray();
            $availableStatus = [];
        } elseif ($user && $user->isAdmin()) {
            // Check if user is admin
            // If admin then get all data
            $assets          = Asset::where('current_state', 'like', $status)->paginate(10)->toArray();
            $availableStatus = Asset::select('current_state')->distinct()->get()->toArray();
        } else {
            // If not admin then get Available
            $assets          = Asset::where('current_state', 'Available')->paginate(10)->toArray();
            $availableStatus = [];
        }

        // Send response
        return $this->sendResponse(['assets' => $assets, 'availableStates' => $availableStatus], 'Assets retrieved successfully.');
    }

    /**
     * Retrieve paginated user array
     *
     * @param  \App\Models\Asset  $asset
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function findAsset(Asset $asset)
    : JsonResponse {
        // Get data
        $asset->toArray();

        // Send response
        return $this->sendResponse($asset, 'Asset retrieved successfully.');
    }

    /**
     * Create new asset
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    : JsonResponse {
        $requestData = $request->all();

        $requestData = array_merge($requestData, ['current_state' => 'Available', 'assignee_id' => null]);

        // Validation rules for the attributes
        $validatedData = $this->getValidatedData($requestData);

        // Validation fail response
        if ($validatedData->fails()) {
            return $this->sendError('Validation Error.', $validatedData->errors(), 422);
        }

        // Create new asset
        $asset = new Asset;
        $this->setAssetAttributes($requestData, $asset);

        // Delete types without relationship
        $this->deleteTypesWithoutRelationship();

        // Send response
        return $this->sendResponse($asset, 'Type created successfully.', 201);
    }

    /**
     * Update asset details
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asset  $asset
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Asset $asset)
    : JsonResponse {
        $requestData = $request->all();

        // Validation rules for the attributes
        $validatedData = $this->getValidatedData($requestData);

        // Validation fail response
        if ($validatedData->fails()) {
            return $this->sendError('Validation Error.', $validatedData->errors(), 422);
        }

        // Updating asset
        $this->setAssetAttributes($requestData, $asset);
        $asset->refresh();

        // Send response
        return $this->sendResponse($asset, 'Asset updated successfully.', 201);
    }

    /**
     * Reserve asset
     *
     * @param  \App\Models\Asset  $asset
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reservation(Asset $asset)
    : JsonResponse {
        if ($asset->current_state === 'Available') {
            $asset->current_state = 'Reserved';
            $asset->assignee_id   = (int) Auth::id();
            $asset->save();
            $asset->refresh();

            // Send response
            return $this->sendResponse($asset, 'Asset reserved successfully.', 201);
        }

        return $this->sendResponse($asset, "Asset unavailable to reserve.", 201);
    }

    /**
     * Approve reservation
     *
     * @param  \App\Models\Asset  $asset
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Asset $asset)
    : JsonResponse {
        if ($asset->current_state === 'Reserved') {
            $asset->current_state = 'Allocated';
            $asset->save();
            $asset->refresh();

            // Send response
            return $this->sendResponse($asset, 'Reservation approved successfully.', 201);
        }

        // Send response
        return $this->sendResponse($asset, "Reservation can't be approved.", 201);
    }

    /**
     * Return asset
     *
     * @param  \App\Models\Asset  $asset
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnAsset(Asset $asset)
    : JsonResponse {
        $user = Auth::user();

        if ($user === null) {
            return $this->sendResponse([], 'User not authed.', 401);
        }


        if ($asset->current_state !== 'Available' && ($user->isAdmin() || $asset->assignee_id === Auth::id())) {
            $asset->current_state = 'Available';
            $asset->assignee_id   = null;
            $asset->save();
            $asset->refresh();

            // Send response
            return $this->sendResponse($asset, 'Asset '.$asset->serial_number.' returned successfully.', 201);
        }

        if ( ! $user->isAdmin() && $asset->assignee_id !== Auth::id()) {
            // Send response
            return $this->sendResponse($asset, "Unauthorised request.", 401);
        }

        // Send response
        return $this->sendResponse($asset, "Asset can't be returned.", 201);
    }

    /**
     * Delete asset
     *
     * @param  \App\Models\Asset  $asset
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Asset $asset)
    : JsonResponse {
        // Delete asset
        $asset->delete();

        // Send response
        return $this->sendResponse($asset, 'Asset deleted successfully.', 204);
    }

    /**
     * Backend validation rules
     *
     * @param  array  $requestData
     *
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
     */
    private function getValidatedData(array $requestData)
    {
        return Validator::make($requestData, [
            'type_id'       => 'required',
            'serial_number' => 'required|string',
            'current_state' => 'required|string|in:Available,Allocated,Damaged,Reserved',
            'assignee_id'   => 'required_if:current_state,==,Allocated,Reserved|numeric|nullable',
        ]);
    }

    /**
     * Type attribute housekeeping
     *
     * @return void
     */
    private function deleteTypesWithoutRelationship()
    : void
    {
        // Deletes all types without relationship to asset. It is to maintain housekeeping in resource Types
        Type::leftJoin('assets', function ($q) {
            $q->on('types.id', 'assets.type_id');
        })->whereNull('assets.id')->delete();
    }

    /**
     * @param  array  $requestData
     * @param  \App\Models\Asset  $asset
     */
    private function setAssetAttributes(array $requestData, Asset $asset)
    : void {
        $asset->type_id       = $requestData['type_id'];
        $asset->serial_number = $requestData['serial_number'];
        $asset->current_state = $requestData['current_state'];
        $asset->assignee_id   = $requestData['assignee_id'];
        $asset->save();
    }
}
