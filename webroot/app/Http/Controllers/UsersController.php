<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UsersController extends BaseController
{
    /**
     * Retrieve paginated user array
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    : JsonResponse
    {
        $users = User::with('assets')->paginate(10)->toArray();

        return $this->sendResponse($users, 'Users retrieved successfully.');
    }

    /**
     * Retrieve user list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userList()
    : JsonResponse
    {
        $users = User::all()->toArray();

        return $this->sendResponse($users, 'Users retrieved successfully.');
    }

    /**
     * Retrieve my profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myProfile()
    : JsonResponse
    {
        $users = User::where('id', Auth::id())->first();

        return $this->sendResponse(['data' => ['0' => $users]], 'User retrieved successfully.');
    }

    /**
     * Update my profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMyProfile(Request $request, User $user)
    {
        if ($user->id === Auth::id()) {
            $requestData = $request->all();

            // Validation rules for the attributes
            $validatedData = Validator::make($requestData, [
                'name'     => 'required|max:255|min:3',
                'email'     => 'required|email|max:255|min:8',
                'password'  => [
                    'max:255',
                    Password::min(8)->letters()->mixedCase()->numbers()->symbols()->uncompromised(),
                ],
            ]);
            // Validation fail response
            if ($validatedData->fails()) {
                return $this->sendError('Validation Error.', $validatedData->errors(), 422);
            }

            // Updating user
            $user->name  = $requestData['name'];
            $user->email = $requestData['email'];
            if (array_key_exists('password', $requestData) && $requestData['password'] !== null && $requestData['password'] !== '') {
                $user->password = $requestData['password'];
            }
            $user->save();
            $user->refresh();

            // Send response
            return $this->sendResponse($user, 'User updated successfully.', 201);
        }

        return $this->sendError('Unauthorised', 'Unauthorised action!', 401);
    }

    /**
     * Update user details
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    : JsonResponse {
        $requestData = $request->all();

        // Validation rules for the attributes
        $validatedData = Validator::make($requestData, [
            'name'     => 'required|max:255|min:3',
            'email'     => 'required|email|max:255|min:8',
            'password'  => [
                'max:255',
                Password::min(8)->letters()->mixedCase()->numbers()->symbols()->uncompromised(),
            ],
            'is_admin' => 'required|bool',
        ]);
        // Validation fail response
        if ($validatedData->fails()) {
            return $this->sendError('Validation Error.', $validatedData->errors(), 422);
        }

        // Updating user
        $user->name     = $requestData['name'];
        $user->email    = $requestData['email'];
        $user->is_admin = $requestData['is_admin'];
        if (array_key_exists('password', $requestData) && $requestData['password'] !== null && $requestData['password'] !== '') {
            $user->password = $requestData['password'];
        }
        $user->save();
        $user->refresh();

        // Send response
        return $this->sendResponse($user, 'User updated successfully.', 201);
    }

    /**
     * Delete user
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    : JsonResponse {
        // Delete user
        $user->delete();

        // Send response
        return $this->sendResponse($user, 'User deleted successfully.', 204);
    }
}
