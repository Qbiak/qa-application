<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Http;
use phpDocumentor\Reflection\Types\Boolean;

class Recaptcha implements Rule
{
    private const  URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return mixed
     */
    public function passes($attribute, $value)
    : mixed {
        return Http::asForm()->post(self::URL, [
            'secret'   => config('services.recaptcha.secret'),
            'response' => $value,
            'remoteip' => request()->ip(),
        ])->json();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    : string
    {
        return 'The validation error - Captcha';
    }
}
