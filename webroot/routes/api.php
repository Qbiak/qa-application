<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AssetsController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\TypesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum', 'request.logging'])->group(function () {
    Route::get('/assets/{status}', [AssetsController::class, 'index'])->middleware('throttle:100,1');
    Route::patch('/assets/reservation/{asset}', [AssetsController::class, 'reservation'])->middleware('throttle:100,1');
    Route::patch('/assets/return/{asset}', [AssetsController::class, 'returnAsset'])->middleware('throttle:100,1');
    Route::get('/my-profile', [UsersController::class, 'myProfile'])->middleware('throttle:100,1');
    Route::put('/update-my-profile/{user}', [UsersController::class, 'updateMyProfile'])->middleware('throttle:100,1');

    Route::middleware('admin')->group(function () {
        Route::get('/users', [UsersController::class, 'index'])->middleware('throttle:100,1');
        Route::get('/users/list', [UsersController::class, 'userList'])->middleware('throttle:100,1');
        Route::apiResource('/assets', AssetsController::class)->only(['update', 'destroy', 'store'])->middleware('throttle:100,1');
        Route::get('/assets/find/{asset}', [AssetsController::class, 'findAsset'])->middleware('throttle:100,1');
        Route::apiResource('/users', UsersController::class)->only(['update', 'destroy'])->middleware('throttle:100,1');
        Route::post('/types', [TypesController::class, 'store'])->middleware('throttle:100,1');
        Route::get('/types/list', [TypesController::class, 'index'])->middleware('throttle:100,1');
        Route::patch('/assets/approve/{asset}', [AssetsController::class, 'approve'])->middleware('throttle:100,1');
    });
});

