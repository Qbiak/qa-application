<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_default_user_is_not_an_admin()
    {
        // Create non-admin user
        $user = User::factory()->create();

        /**
         * Assert
         */
        $this->assertFalse($user->isAdmin());
    }

    /** @test */
    public function an_admin_user_is_an_admin()
    {
        // Set admin on created user
        $admin = User::factory()->setAdmin()->create();

        /**
         * Assert
         */
        $this->assertTrue($admin->isAdmin());
    }
}
