<?php

namespace Tests\Unit;

use App\Models\Type;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class TypeTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    /** @test */
    public function a_type_can_be_created()
    {
        // Create type
        $type             = new Type;
        $type->asset_type = 'Laptop';
        $type->save();

        /**
         * Assert
         */
        $this->assertCount(1, Type::all());
    }
}
