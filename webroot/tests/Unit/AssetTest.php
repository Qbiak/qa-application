<?php

namespace Tests\Unit;

use App\Models\Asset;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AssetTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    protected $asset;

    /** @test */
    public function an_asset_can_be_added_to_the_database()
    {
        //disable foreign key constrain
        Schema::disableForeignKeyConstraints();

        $asset                = new Asset;
        $asset->type_id       = 1;
        $asset->serial_number = 'SR00001';
        $asset->current_state = 'Allocated';
        $asset->assignee_id   = 1;
        $asset->save();

        //enable foreign key constrain
        Schema::enableForeignKeyConstraints();

        /**
         * Assert
         */
        $this->assertDatabaseHas('assets', [
            'type_id'       => $asset->type_id,
            'serial_number' => $asset->serial_number,
            'current_state' => $asset->current_state,
            'assignee_id'   => $asset->assignee_id,
        ]);

        $this->assertCount(1, Asset::all());
    }
}
