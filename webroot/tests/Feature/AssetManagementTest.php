<?php

namespace Tests\Feature;

use App\Models\Asset;
use App\Models\User;
use App\Models\Type;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class AssetManagementTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     *
     * @return void
     */
    public function a_type_id_is_required()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create asset
        $response = $this->post('api/assets', array_merge($this->assetAttributesSetA(), ['type_id' => '']));

        /**
         * Assert
         */
        $response->assertJson([
            'message' => 'Validation Error.',
            'errors'  => ['type_id' => [0 => 'The type id field is required.']],
        ]);
    }


    /**
     * @test
     *
     * @return void
     */
    public function a_serial_number_is_required()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create asset
        $response = $this->post('api/assets', array_merge($this->assetAttributesSetA(), ['serial_number' => '']));

        /**
         * Assert
         */
        $response->assertJson([
            'message' => 'Validation Error.',
            'errors'  => ['serial_number' => [0 => 'The serial number field is required.']],
        ]);
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_current_state_is_auto_assigned()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create asset
        $response = $this->post('api/assets', array_merge($this->assetAttributesSetA(), ['current_state' => '']));

        /**
         * Assert
         */
        $response->assertStatus(201)->assertJson(['message' => 'Type created successfully.']);
        $this->assertCount(1, Asset::all());
        $response->assertJson(['data' => ['current_state' => 'Available'],]);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_assignee_id_is_nullable()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create user
        User::factory()->count(2)->create();

        //create asset
        $response = $this->post('api/assets', array_merge($this->assetAttributesSetA(), ['assignee_id' => null]));

        /**
         * Assert
         */

        $response->assertStatus(201)->assertJson(['message' => 'Type created successfully.']);
        $this->assertCount(1, Asset::all());
    }

    /**
     * @test
     *
     * @return void
     */
    /** @test */
    public function an_asset_can_be_updated_by_admin()
    : void
    {
        //login admin
        $this->loginAdmin();

        $response = $this->updateAsset();

        /**
         * Assert
         */
        $this->assertEquals(Type::where('asset_type', 'Mobile')->first()->id, Asset::first()->type_id);
        $this->assertEquals('SR00002', Asset::first()->serial_number);
        $this->assertEquals('Reserved', Asset::first()->current_state);
        $this->assertEquals(2, Asset::first()->assignee_id);
        $response->assertStatus(201);
    }

    /**
     * @test
     *
     * @return void
     */
    /** @test */
    public function an_asset_can_be_updated_by_user()
    : void
    {
        //login user
        $this->loginUser();

        $response = $this->updateAsset();

        /**
         * Assert
         */
        $response->assertStatus(401);
        $this->assertEquals(Type::where('asset_type', 'Laptop')->first()->id, Asset::first()->type_id);
        $this->assertEquals('SR00001', Asset::first()->serial_number);
        $this->assertEquals('Allocated', Asset::first()->current_state);
        $this->assertEquals(1, Asset::first()->assignee_id);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_asset_can_be_deleted_by_admin()
    : void
    {
        //login admin
        $this->loginAdmin();

        $response = $this->deleteAsset();

        /**
         * Assert
         */
        $this->assertCount(0, Asset::all());
        $response->assertStatus(204);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_asset_can_not_be_deleted_by_user()
    : void
    {
        //login user
        $this->loginUser();

        $response = $this->deleteAsset();

        /**
         * Assert
         */
        $this->assertCount(1, Asset::all());
        $response->assertStatus(401);
    }

    /**
     * @test
     *
     * @return void
     */
    public function assets_list_can_be_retrieved_by_admin()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create user
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());
        Asset::create($this->assetAttributesSetB());

        //retrieve assets
        $response = $this->get('api/assets/All');

        /**
         * Assert
         */
        $response->assertJsonFragment(array_merge($this->assetAttributesSetA(), ['type_id' => Type::where('asset_type', 'Laptop')->first()->id]));
        $response->assertJsonFragment(array_merge($this->assetAttributesSetB(), ['type_id' => Type::where('asset_type', 'Mobile')->first()->id]));
        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function available_assets_can_be_retrieved_by_user()
    : void
    {
        //login user
        $this->loginUser();

        //create user
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());
        Asset::create($this->assetAttributesSetB());
        Asset::create(array_merge($this->assetAttributesSetB(),
            ['serial_number' => 'SCNUnique10', 'current_state' => 'Available', 'assignee_id' => null, 'type_id' => 'Headset']));

        //retrieve assets
        $response = $this->get('api/assets/All');

        /**
         * Assert
         */
        $response->assertJsonMissing(array_merge($this->assetAttributesSetA(), ['type_id' => Type::where('asset_type', 'Laptop')->first()->id]));
        $response->assertJsonMissing(array_merge($this->assetAttributesSetB(), ['type_id' => Type::where('asset_type', 'Mobile')->first()->id]));
        $response->assertJsonFragment(array_merge($this->assetAttributesSetB(), [
                'serial_number' => 'SCNUnique10',
                'current_state' => 'Available',
                'assignee_id'   => null,
                'type_id'       => Type::where('asset_type', 'Headset')->first()->id,
            ]));
        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function own_assets_can_be_retrieved_by_user()
    : void
    {
        //login user
        $this->loginUser();

        //create user
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());
        Asset::create($this->assetAttributesSetB());
        Asset::create(array_merge($this->assetAttributesSetB(),
            ['serial_number' => 'SCNUnique10', 'current_state' => 'Available', 'assignee_id' => null, 'type_id' => 'Headset']));

        //retrieve assets
        $response = $this->get('api/assets/own');

        /**
         * Assert
         */
        $response->assertJsonFragment(array_merge($this->assetAttributesSetA(), ['type_id' => Type::where('asset_type', 'Laptop')->first()->id]));
        $response->assertJsonMissing(array_merge($this->assetAttributesSetB(), ['type_id' => Type::where('asset_type', 'Mobile')->first()->id]));
        $response->assertJsonMissing(array_merge($this->assetAttributesSetB(), [
            'serial_number' => 'SCNUnique10',
            'current_state' => 'Available',
            'assignee_id'   => null,
            'type_id'       => Type::where('asset_type', 'Headset')->first()->id,
        ]));
        $response->assertStatus(200);
    }


    /**
     * @test
     *
     * @return void
     */
    public function user_can_retrieve_own_assets()
    : void
    {
        //login user
        $this->loginUser();

        //create user
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());
        Asset::create($this->assetAttributesSetB());
        Asset::create(array_merge($this->assetAttributesSetB(),
            ['serial_number' => 'SCNUnique10', 'current_state' => 'Available', 'assignee_id' => null]));

        //retrieve assets
        $response = $this->get('api/assets/own');

        /**
         * Assert
         */
        $response->assertJsonFragment(array_merge($this->assetAttributesSetA(), ['type_id' => Type::where('asset_type', 'Laptop')->first()->id]));
        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_new_type_is_automatically_added()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create user
        User::factory()->count(2)->create();

        //create asset
        $this->post('api/assets', $this->assetAttributesSetA());

        $type  = Type::first();
        $asset = Asset::first();

        /**
         * Assert
         */
        $this->assertEquals($type->id, $asset->type_id);
        $this->assertCount(1, Asset::all());
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_user_can_reserve_available_asset()
    : void
    {
        //login admin
        $this->loginUser();

        //create users
        User::factory()->count(2)->create();

        //create asset
        Asset::create(array_merge($this->assetAttributesSetB(), ['current_state' => 'Available']));

        //find asset id
        $asset = Asset::first()->id;

        //request asset
        $this->patch('api/assets/reservation/'.$asset);

        /**
         * Assert
         */
        $this->assertCount(1, Asset::all());
        $this->assertEquals('Reserved', Asset::first()->current_state);
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_user_can_not_reserve_unavailable_asset()
    : void
    {
        //login user
        $this->loginUser();

        //create user
        User::factory()->create();

        //create unavailable asset
        Asset::create($this->assetAttributesSetA());

        //find asset ID
        $assetId = Asset::first()->id;

        //reserve asset
        $this->patch('/api/assets/reservation/'.$assetId);


        /**
         * Assert
         */
        $this->assertEquals(Asset::first()->current_state, $this->assetAttributesSetA()['current_state']);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_admin_can_approve_reserved_asset()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create users
        User::factory()->count(2)->create();

        //create asset
        Asset::create(array_merge($this->assetAttributesSetB()));

        //find asset id
        $asset = Asset::first()->id;

        //approve asset
        $this->patch('api/assets/approve/'.$asset);

        /**
         * Assert
         */
        $this->assertCount(1, Asset::all());
        $this->assertEquals('Allocated', Asset::first()->current_state);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_admin_can_not_approve_unavailable_asset()
    : void
    {
        //login user
        $this->loginAdmin();

        //create user
        User::factory()->create();

        //create unavailable asset
        Asset::create($this->assetAttributesSetA());

        //find asset ID
        $assetId = Asset::first()->id;

        //approve unavailable asset
        $this->patch('/api/assets/approve/'.$assetId);


        /**
         * Assert
         */
        $this->assertEquals(Asset::first()->current_state, $this->assetAttributesSetA()['current_state']);
    }


    /**
     * @test
     *
     * @return void
     */
    public function a_user_can_approve_reserved_asset()
    : void
    {
        //login admin
        $this->loginUser();

        //create users
        User::factory()->count(2)->create();

        //create asset
        Asset::create(array_merge($this->assetAttributesSetB()));

        //find asset id
        $asset = Asset::first()->id;

        //approve asset
        $this->patch('api/assets/approve/'.$asset);

        /**
         * Assert
         */
        $this->assertCount(1, Asset::all());
        $this->assertEquals('Reserved', Asset::first()->current_state);
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_type_without_relationship_is_deleted()
    : void
    {
        $this->loginAdmin();

        //create type
        $this->post('api/types', ['asset_type' => 'No relation']);

        //create asset
        $this->post('api/assets', $this->assetAttributesSetA());


        /**
         * Assert
         */
        $this->assertDatabaseMissing('types', ['asset_type' => 'No relation']);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_asset_is_unassigned_when_damaged()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create user
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());

        $this->patch(Asset::first()->path(), array_merge($this->assetAttributesSetA(), ['current_state' => 'Damaged']));


        /**
         * Assert
         */
        $this->assertDatabaseHas('assets', ['assignee_id' => null]);
    }

    /**
     * @test
     *
     * @return void
     */
    public function an_asset_is_unassigned_when_made_available()
    : void
    {
        //login admin
        $this->loginAdmin();

        //create user
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());

        $this->patch(Asset::first()->path(), array_merge($this->assetAttributesSetA(), ['current_state' => 'Available']));


        /**
         * Assert
         */
        $this->assertDatabaseHas('assets', ['assignee_id' => null]);
    }

    /**
     * @return string[]
     */
    private function assetAttributesSetA()
    : array
    {
        return [
            'type_id'       => 'Laptop',
            'serial_number' => 'SR00001',
            'current_state' => 'Allocated',
            'assignee_id'   => 1,
        ];
    }

    /**
     * @return string[]
     */
    private function assetAttributesSetB()
    : array
    {
        return [
            'type_id'       => 'Mobile',
            'serial_number' => 'SR00002',
            'current_state' => 'Reserved',
            'assignee_id'   => 2,
        ];
    }

    /**
     * @return \Illuminate\Testing\TestResponse
     */
    private function deleteAsset()
    : \Illuminate\Testing\TestResponse
    {
        //create user
        User::factory()->create();

        //create asset
        Asset::create($this->assetAttributesSetA());

        //find first asset
        $asset = Asset::first();

        /**
         * Assert
         */
        $this->assertCount(1, Asset::all());

        //delete asset
        $response = $this->delete($asset->path());

        return $response;
    }


    /**
     * @return \Illuminate\Testing\TestResponse
     */
    private function updateAsset()
    : \Illuminate\Testing\TestResponse
    {
        //create 2 users
        User::factory()->count(2)->create();

        //create asset
        Asset::create($this->assetAttributesSetA());

        //update asset
        $response = $this->patch(Asset::first()->path(), $this->assetAttributesSetB());

        return $response;
    }

    private function loginAdmin()
    : void
    {
        //create admin
        $admin = User::factory()->setAdmin()->create();

        // login your user in system
        $this->be($admin);
    }

    private function loginUser()

    {
        //create user
        $user = User::factory()->create();

        //login user in system
        $this->be($user);

        return $user;
    }
}
