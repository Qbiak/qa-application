<?php

namespace Tests\Feature;

use App\Models\Type;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TypeManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_type_can_be_created()
    {
        // Login admin
        $this->loginAdmin();

        // Create new type
        $this->post('api/types', [
            'asset_type' => 'Laptop',
        ]);

        /**
         * Assert
         */
        $this->assertCount(1, Type::all());
    }

    /** @test */
    public function an_asset_type_is_required()
    {
        $this->loginAdmin();

        // Create asset
        $response = $this->post('api/types', ['asset_type' => '']);


        /**
         * Assert
         */
        $response->assertJson([
            'message' => 'Validation Error.',
            'errors'  => ['asset_type' => [0 => 'The asset type field is required.']],
        ]);
    }

    private function loginAdmin()
    : void
    {
        // Create user
        $user = User::factory()->setAdmin()->create();

        // Login your user in system
        $this->be($user);
    }
}
