<?php

namespace Tests\Feature;

use App\Models\Asset;
use App\Models\User;
use App\Models\Type;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class BrokenAuthenticationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     *
     * @return void
     */
    public function a_password_has_rules()
    : void
    {
        // Register user
        $response = $this->post('/register', array_merge($this->data(), ['password' => '123456']));

        /**
         * Assert
         */
        $response->assertJson([
            'message' => 'Validation Error.',
            'errors'  => [
                'password' => [
                    'The password confirmation does not match.',
                    'The password must be at least 8 characters.',
                    'The password must contain at least one uppercase and one lowercase letter.',
                    'The password must contain at least one letter.',
                    'The password must contain at least one symbol.',
                ],
            ],
        ]);
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_password_can_not_appear_in_data_leaks()
    : void
    {
        // Register user
        $response = $this->post('/register', array_merge($this->data(), ['password' => 'P4$$w0rd']));

        /**
         * Assert
         */
        $response->assertJson([
            'message' => 'Validation Error.',
            'errors'  => [
                'password' => [
                    'The password confirmation does not match.',
                    'The given password has appeared in a data leak. Please choose a different password.',
                ],
            ],
        ]);
    }

    /**
     * @test
     *
     * @return void
     */
    public function a_recaptcha_is_required()
    : void
    {
        // Register user
        $response = $this->post('/register', array_merge($this->data(), ['recaptcha' => '']));

        /**
         * Assert
         */
        $response->assertJson([
            'message' => 'Validation Error.',
            'errors'  => ['recaptcha' => [0 => 'The recaptcha field is required.']],
        ]);
    }

    /**
     * @return string[]
     */
    private function data()
    : array
    {
        return [
            'name'                  => 'Joe',
            'email'                 => 'testemail@test.com',
            'password'              => 'password-teAt-complex4567',
            'password_confirmation' => 'password-teAt-complex4567',
            'recaptcha'             => 'pass',
        ];
    }
}
