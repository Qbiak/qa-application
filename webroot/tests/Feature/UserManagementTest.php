<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class UserManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_register()
    : void
    {
        // Register user
        $response = $this->post('/register', $this->data());

        // Find first user
        $user = User::first();

        /**
         * Assert
         */
        $response->assertStatus(201)->assertJson(['message' => 'Registered Successfully']);
        $this->assertDatabaseHas('users', ['name' => 'Joe']);
        $this->assertDatabaseHas('users', ['email' => 'testemail@test.com']);
        $this->assertTrue(Hash::check('password-teAt-complex4567', $user->password));
    }



    /** @test */
    public function a_user_can_log_in()
    : void
    {
        // Create user
        $user = User::create($this->data());

        // User log in
        $response = $this->post('/login', $this->data());

        /**
         * Assert
         */
        $response->assertStatus(200)->assertJson(['data' => User::first()->toArray()]);
    }


    /** @test */
    public function invalid_login_details()
    : void
    {
        // Create user
        $user = User::create($this->data());

        // User log in
        $response = $this->post('/login', array_merge($this->data(), ['email' => 'incorrect@email.com']));

        /**
         * Assert
         */
        $response->assertStatus(401)->assertJson(['message' => 'Invalid Login Details']);
    }

    /** @test */
    public function a_user_can_log_out()
    : void
    {
        // Create user
        $user = User::factory()->make();

        // User log in
        $this->actingAs($user);

        // Logout
        $response = $this->post('/logout');

        /**
         * Assert
         */
        $response->assertStatus(200)->assertJson(['message' => 'Logged Out Successfully']);
        $this->assertGuest();
    }

    /** @test */
    public function a_user_can_be_deleted_by_admin()
    {
        // Create user
        User::factory()->create();

        // Create admin
        $admin = User::factory()->setAdmin()->create();

        // Login admin
        $this->actingAs($admin);

        // Find first user
        $user = User::first();

        /**
         * Assert
         */
        $this->assertCount(2, User::all());

        // Delete user
        $response = $this->deleteJson($user->path());

        /**
         * Assert
         */
        $this->assertCount(1, User::all());
        $response->assertStatus(204);
    }

    /** @test */
    public function a_user_can_not_be_deleted_by_user()
    {
        // Create user
        User::factory()->create();

        // Create second user
        $user = User::factory()->create();

        // Login your user in system
        $this->actingAs($user);

        // Find first user
        $user = User::first();

        /**
         * Assert
         */
        $this->assertCount(2, User::all());

        // Delete user
        $response = $this->delete($user->path());

        /**
         * Assert
         */
        $this->assertCount(2, User::all());
        $response->assertStatus(401);
    }

    /**
     * @test
     *
     * @return void
     */
    public function user_can_retrieve_personal_data()
    : void
    {
        //login user
        $this->loginUser();

        //create user
        User::factory()->count(2)->create();

        //retrieve profile data
        $response = $this->get('api/my-profile/');

        /**
         * Assert
         */
        $response->assertJsonFragment(['name' => User::where('id', 1)->first()->name, 'email' => User::where('id', 1)->first()->email]);
        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function user_can_save_personal_data()
    : void
    {
        //login user
        $this->loginUser();

        //retrieve profile data
        $response = $this->put('api/update-my-profile/1', $this->data());

        /**
         * Assert
         */
        $response->assertJsonFragment([
            'name'  => 'Joe',
            'email' => 'testemail@test.com',
        ]);
        $response->assertStatus(201);
    }

    /**
     * @return string[]
     */
    private function data()
    : array
    {
        return [
            'name'                  => 'Joe',
            'email'                 => 'testemail@test.com',
            'password'              => 'password-teAt-complex4567',
            'password_confirmation' => 'password-teAt-complex4567',
            'recaptcha'             => 'pass',
        ];
    }

    private function loginUser()

    {
        //create user
        $user = User::factory()->create();

        //login user in system
        $this->be($user);

        return $user;
    }
}
