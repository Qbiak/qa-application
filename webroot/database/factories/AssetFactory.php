<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AssetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type_id'       => $this->faker->randomElement($this->assetTypes()),
            'serial_number' => 'SN-WN-'.$this->faker->unique->numberBetween(10000000, 99999999),
            'current_state' => $this->faker->randomElement($this->states()),
            'assignee_id'   => User::factory()
        ];
    }

    private function states()
    : array
    {
        return [
            'Available',
            'Allocated',
            'Damaged',
            'Reserved',
        ];
    }

    private function assetTypes()
    : array
    {
        return [
            'Laptop',
            'Mobile',
            'Headset',
            'Tablet',
        ];
    }
}
