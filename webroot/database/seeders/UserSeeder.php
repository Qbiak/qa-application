<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    : void
    {
        User::factory()->state([
            'name' => 'Joe Blogs', 'email' => 'admin@test.com',
        ])->setAdmin()->create();

        User::factory()->state([
            'name' => 'Fred Blogs', 'email' => 'user@test.com',
        ])->create();
    }
}
