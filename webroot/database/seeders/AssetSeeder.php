<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\User;
use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    : void
    {
        Asset::factory()->state([
            'current_state' => 'Available',
            'assignee_id'   => null,
        ])->count(2)->create();

        Asset::factory()->state([
            'current_state' => 'Damaged',
            'assignee_id'   => null,
        ])->count(2)->create();

        Asset::factory()->state([
            'current_state' => 'Allocated',
            'assignee_id'   => User::where('email', 'user@test.com')->first()->id,
        ])->count(2)->create();

        Asset::factory()->state([
            'current_state' => 'Reserved',
            'assignee_id'   => User::where('email', 'user@test.com')->first()->id,
        ])->count(2)->create();

        Asset::factory()->state([
            'current_state' => 'Available',
            'assignee_id'   => null,
        ])->count(15)->create();

        Asset::factory()->state([
            'current_state' => 'Damaged',
            'assignee_id'   => null,
        ])->count(15)->create();

        Asset::factory()->state([
            'current_state' => 'Allocated',
        ])->count(15)->create();

        Asset::factory()->state([
            'current_state' => 'Reserved',
        ])->count(15)->create();
    }
}
