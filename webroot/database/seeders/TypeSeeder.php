<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    : void
    {
        DB::table('types')->insert([
            'asset_type' => 'Laptop',
        ]);

        DB::table('types')->insert([
            'asset_type' => 'Mobile',
        ]);

        DB::table('types')->insert([
            'asset_type' => 'Headset',
        ]);

        DB::table('types')->insert([
            'asset_type' => 'Tablet',
        ]);
    }
}
